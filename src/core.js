import Controls from './controls'

export default function (options) {
  let cy = this
  return new Controls(cy, options)
}
