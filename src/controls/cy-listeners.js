function addCytoscapeListeners () {
  let { cy } = this

  // show control on hover
  this.addListener(cy, 'mouseover', 'node, edge', e => {
    this.show(e.target)
  })

  // show control
  // trigger control
  this.addListener(cy, 'tap', 'node, edge', e => {
    const ele = e.target
    if (ele.anySame(this.controlNodes)) {
      this.trigger(ele)
    } else {
      this.show(ele)
    }
  })

  // hide control when any node moved
  this.addListener(cy, 'position', 'node', () => {
    this.hide()
  })

  // hide control if source node or edge is removed
  this.addListener(cy, 'remove', e => {
    if (e.target.same(this.sourceElement)) {
      this.hide()
    }
  })

  return this
}

export default { addCytoscapeListeners }
